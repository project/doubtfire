<?php
/**
 * @file
 * Favourites include file for the doubtfire module.
 *
 * Contains aditional functionailty for the favourites feature.
 */

/**
 * Toggles the user in the favourites list.
 *
 * @param obj $user
 *    User to be favourited.
 */
function doubtfire_toggle_favourite_user($user) {
  if (isset($_SESSION[DOUBTFIRE_ROOT_USER_KEY]) && empty($_SESSION[DOUBTFIRE_ROOT_USER_KEY])) {
    drupal_not_found();
  }
  $root_uid = isset($_SESSION[DOUBTFIRE_ROOT_USER_KEY]) ? $_SESSION[DOUBTFIRE_ROOT_USER_KEY] : 0;

  if (doubtfire_user_is_favourited($user->uid)) {
    $success = db_delete('doubtfire_favourites')
      ->condition('uid', $user->uid)
      ->condition('admin_uid', $root_uid)
      ->execute();
    if ($success) {
      drupal_set_message(check_plain(doubtfire_username_for_user($user) . ' has been removed from your favourites'));
      drupal_goto($_SERVER['HTTP_REFERER']);
    }
    else {
      drupal_set_message(t('There was an error removing the user from your favourites'), 'error');
      drupal_goto($_SERVER['HTTP_REFERER']);
    }
  }
  else {
    $success = db_insert('doubtfire_favourites')
      ->fields(array(
        'uid' => $user->uid,
        'admin_uid' => $root_uid,
      ))->execute();

    if ($success) {
      drupal_set_message(check_plain(doubtfire_username_for_user($user) . ' has been added to your favourites'));
      drupal_goto($_SERVER['HTTP_REFERER']);
    }
    else {
      drupal_set_message(t('There was an error adding the user to your favourites'), 'error');
      drupal_goto($_SERVER['HTTP_REFERER']);
    }
  }
}

/**
 * Checks to see if the user has already been favourited.
 *
 * @param int $uid
 *    User to check.
 *
 * @return bool
 *    Returns TRUE if user is favourited by current doubtfire root user
 */
function doubtfire_user_is_favourited($uid) {
  $data = db_select('doubtfire_favourites', 'dbf')
    ->fields('dbf')
    ->condition('admin_uid', isset($_SESSION[DOUBTFIRE_ROOT_USER_KEY]) ? $_SESSION[DOUBTFIRE_ROOT_USER_KEY] : 0)
    ->condition('uid', $uid)
    ->execute()
    ->fetchAssoc();
  return (!empty($data));
}

/**
 * Gets the menu text for the button.
 *
 * @param int $uid
 *    Checks the user has been favourited.
 *
 * @return string
 *    Returns the appropriate text label
 */
function doubtfire_favourite_menu_label($uid) {
  return (doubtfire_user_is_favourited($uid))
    ? '<span class="favourite-star active">☆</span>'
    : '<span class="favourite-star">☆</span>';
}

/**
 * Returns all favourites for user.
 *
 * @param int $auid
 *    Defaults to ROOT USER in session.
 *
 * @return array
 *    Returns an array of users
 */
function doubtfire_get_favourites($auid = NULL) {
  if (!$auid) {
    global $user;
    $auid = (!empty($_SESSION[DOUBTFIRE_ROOT_USER_KEY]))
      ? $_SESSION[DOUBTFIRE_ROOT_USER_KEY]
      : $user->uid;
  }
  $data = db_select('doubtfire_favourites', 'dbf')
    ->fields('dbf')
    ->condition('admin_uid', $auid)
    ->execute()
    ->fetchAll();
  return $data;
}
