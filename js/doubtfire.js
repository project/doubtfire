/**
 * @file
 * JS for the Doubtfire module.
 */

 /**
  * Called from template file "doubtfire-search-result.tpl.php" inline onClick.
  */
function closesearch() {
  'use strict';
  var parent = document.getElementById('doubtfire-search-autocomplete');
  while (parent.firstChild) {
    parent.removeChild(parent.firstChild);
  }
}

document.addEventListener('DOMContentLoaded', function () {
  'use strict';
  document.getElementsByClassName('doubtfire-favourites')[0].addEventListener('click', function (event) {
    event.preventDefault();
  });
  if (document.getElementsByClassName('page-admin').length > 0
    && document.getElementsByClassName('doubtfire_enabled').length > 0) {
    document.querySelector('body').style.paddingBottom = "75px";
  }
}, false);
