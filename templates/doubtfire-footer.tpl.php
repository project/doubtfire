<?php

/**
 * @file
 * Template file for the menu items in footer.
 */
?>
<?php foreach($menu_items as $item) : ?>
  <li class="doubtfire-footer-menu-item">
    <?php switch ($item['type']) {

      case 'markup':
        echo $item['#markup'];
        break;

      case 'label':
      ?>
        <i class="<?php echo (isset($item['class'])) ? $item['class'] : ''; ?>">
          <?php echo $item['label']; ?>
        </i>
      <?php
        break;

      default:
      ?>
        <a class="<?php echo (isset($item['class'])) ? $item['class'] : ''; ?>"
          href="<?php echo $item['link']; ?>">
          <?php echo $item['label']; ?>
        </a>
        <?php if (!empty($item['children'])) : ?>
          <ul id="doubtfire-footer-menu-dropdown" class="<?php echo (isset($item['class'])) ? $item['class'] : ''; ?>">
            <?php foreach ($item['children'] as $child) : ?>
              <li class="doubtfire-footer-menu-dropdown-item">
                <a href="<?php echo $child['link']; ?>">
                  <?php echo $child['label']; ?>
                </a>
              </li>
            <?php endforeach; ?>
          </ul>
        <?php endif; ?>
      <?php
        break;

    } ?>
  </li>
<?php endforeach; ?>
