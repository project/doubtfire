<h1>Doubtfire</h1>

<p>The doubtfire module allows users to switch to a different user account to go
undercover. Users can be searched for by username and role and can be favourited
for quick selection of users.</p>

<h2>Installation and Configuration</h2>

<p>Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.</p>

<p>To allow a user role to use the admin bar give the role the 'doubtfire go
undercover' permission.</p>

<p>Enable the doubtfire bar by using the 'Toggle Doubtfire' menu item.</p>

<h2>Doubtfire Menu Bar</h2>

<p>When using doubfire a menu bar will be added to the bottom of the site. This
allows searching, switching and favouriting of users.</p>

<p>To go undercover as a user, search for the specific user and click on their
button. A notification of the user you are currently viewing the site with will
be in the right of the bar along with the 'reveal cover' button.</p>

<p>Reveal Cover: This will switch you back to the original user. It is possible
to switch to another user without revealing cover.</p>

<p>Favourites: While undercover as a user you can use the favourites star to
add the user to your favourites. This will allow you to quickly access the user
in the favourites menu at the left of the bar.</p>

<h2>Help and Support</h2>

<p>This module has been developed by the people at Nudge Digital. For more info
please contact <a href="mailto:doubtfire@nudgedigital.co.uk">doubtfire@nudgedigital.co.uk.</a></p>
