<?php

/**
 * @file
 * Template for the ajax search results which appear below the search form.
 */
?>
<div id="doubtfire-search-autocomplete">
  <h3>Found <?php echo $count; ?> users</h3>
  <a onclick="closesearch()" class="doubtfire-search-close">Clear results</a>
  <ul>
    <?php foreach ($users as $user) : ?>
      <li>
        <a class="grey-bg" href="/doubtfire/login-as/<?php echo $user->uid; ?>"><?php echo $user->username; ?></a>
      </li>
    <?php endforeach; ?>
    <?php if ($overflow > 0) : ?>
        <li class="doubtfire-append"><span>and <?php echo $overflow; ?> more...</span></li>
    <?php endif; ?>
  </ul>
  <?php if ($overflow > 0) : ?>
  <?php endif; ?>
</div>
