# Doubtfire

The doubtfire module allows users to switch to a different user account to go
undercover. Users can be searched for by username and role and can be favourited
 for quick selection of users.

### Installation and Configuration

Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.

* To allow a user role to user the admin bar give the role the 'doubtfire go
undercover' permission.

* Enable the doubtfire bar by using the 'Toggle Doubtfire' menu item.

### Doubtfire Menu Bar

When using doubfire a menu bar will be added to the bottom of the site. This
allows searching, switching and favouriting of users.

 * To go undercover as a user, search for the specific user and click on their
button. A notification of the user you are currently viewing the site with will
be in the right of the bar along with the 'reveal cover' button.

 * Reveal Cover: This will switch you back to the original user. It is possible
 to switch to another user without revealing cover.

 * Favourites: While undercover as a user you can use the favourites star to
 add the user to your favourites. This will allow you to quickly access the user
  in the favourites menu at the left of the bar.

### Help and Support

This module has been developed by the people at Nudge Digital. For more info
please contact doubtfire@nudgedigital.co.uk.

Project Page: -

Issue Queue: -
