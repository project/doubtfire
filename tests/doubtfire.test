<?php

/**
 * @file
 * Test class for Doubtfire module.
 */

/**
 * Test that module creates field and allows values to be set correctly.
 */
class DoubtfireTestCase extends DrupalWebTestCase {

  public static function getInfo() {
    return array(
      'name' => 'Doubtfire Tests',
      'description' => 'Ensures the module functions as designed',
      'group' => 'User',
    );
  }

  /**
   * Creates page and test values.
   */
  public function testCanGoUnderCover() {
    $account = $this->drupalCreateUser(array('bypass node access', 'administer nodes'));

    $this->assertFalse(doubtfire_can_go_undercover($account, mt_rand(10000000, 99999999)), "User doesn't exist");
    $this->assertFalse(doubtfire_can_go_undercover(NULL, $account), "User is empty");
  }

  public function testUserChangesWhenUnderCover() {
    $this->assertEqual(1, $GLOBALS['user']->uid);

    // Create a new user to go undercover as. Doing it this way rather than
    // using drupalCreateUser() to avoid getting notice for missing
    // $user->timezone property.
    $edit = array();
    $edit['name'] = $name = $this->randomName(8);
    $edit['mail'] = $name . '@example.com';
    $this->drupalPost('user/register', $edit, t('Create new account'));

    doubtfire_login_as(user_load(2));

    // Ensure that the logged in user ID has changed.
    $this->assertEqual(2, $GLOBALS['user']->uid);

    doubtfire_login_undo();

    // Ensure that the logged in user ID is back to the original.
    $this->assertEqual(1, $GLOBALS['user']->uid);
  }

}
